#version 120

uniform mat4 modelToCameraMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 modelToClipMatrix;

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)
uniform vec3 scene_ambient;  // rgb

uniform struct light_t {
	vec4 position;    // Camera space
	vec3 diffuse;     // rgb
	vec3 specular;    // rgb
	vec3 attenuation; // (constant, lineal, quadratic)
	vec3 spotDir;     // Camera space
	float cosCutOff;  // cutOff cosine
	float exponent;
} theLights[4];     // MG_MAX_LIGHTS

uniform struct material_t {
	vec3  diffuse;
	vec3  specular;
	float alpha;
	float shininess;
} theMaterial;

attribute vec3 v_position; // Model space
attribute vec3 v_normal;   // Model space
attribute vec2 v_texCoord;

varying vec4 f_color;
varying vec2 f_texCoord;


float lamberFact (in vec3 n, in vec3 l){
	return max (dot(n, l), 0.0);
}

float specular_factor(in vec3 n, in vec3 l, in vec3 v, in float m){

	vec3 r = normalize(2 * dot(n, l) * n - l);

	float RoV = dot(r, v);

	if(RoV > 0.0){
		return pow(RoV, m);
	}else{
		return 0.0;
	}
	
}

void aporte_direccional(in int i, in vec3 l, in vec3 n, in vec3 v, inout vec3 acumulador, inout vec3 i_especular){

	float NoL = lamberFact(n, l);

	if (NoL > 0.0){
		float spec = specular_factor(n, l, v, theMaterial.shininess);

		acumulador += NoL * theMaterial.diffuse * theLights[i].diffuse;		
		i_especular += spec * theMaterial.specular * theLights[i].specular * NoL;
	}


}

void aporte_Posicional(in int i, in vec3 l, in vec3 n, in vec3 v, inout vec3 acumulador, inout vec3 i_especular, in float atenuacion_luz){

	float NoL = lamberFact(n, l);

	if (NoL > 0.0){
		float spec = specular_factor(n, l, v, theMaterial.shininess);

		acumulador += NoL * atenuacion_luz * theMaterial.diffuse * theLights[i].diffuse;
		i_especular += spec * theMaterial.specular * theLights[i].specular * NoL * atenuacion_luz;
	}
}


void aporteSpot(in int i, in vec3 l, in vec3 n, in vec3 v, inout vec3 acumulador, inout vec3 i_especular){

	vec3 s = normalize(theLights[i].spotDir);

	float fact = dot(-l, s);

	float NoL = lamberFact(n, l);

	if (NoL > 0.0){

		if(fact >= theLights[i].cosCutOff){

			float Cspot = 0.0;

			if(fact > 0.0){

				Cspot = pow(fact, theLights[i].exponent);

			}
			
			if(Cspot > 0.0){
				float spec = specular_factor(n, l, v, theMaterial.shininess);

				acumulador += NoL * theMaterial.diffuse * theLights[i].diffuse * Cspot;		
				i_especular += spec * theMaterial.specular * theLights[i].specular * NoL * Cspot;
			}
		
		}
	}
}


void main() {

	vec3 L, N, V;
	vec3 acumulador_difuso = vec3(0.0);
	vec3 acumulador_especular = vec3(0.0);
	vec4 N4 = modelToCameraMatrix * vec4(v_normal, 0.0);

	vec4 positionEye = modelToCameraMatrix * vec4(v_position, 1.0);

	vec4 V4 = (0, 0, 0, 1) -  positionEye;


	N = normalize(N4.xyz); ///en el sistema de coordenadas de la camara (s.c.camara)
	V = normalize(V4.xyz);

	float atenuacion_luz = 1.0;


	for (int i = 0; i < active_lights_n; i++){

		//Luz direccional ??
		// (x, y, z, 0.0)
		if (theLights[i].position.w == 0.0){

			//Vector de la luz direccional
			L = normalize(-1.0 * theLights[i].position.xyz);

			aporte_direccional(i, L, N, V, acumulador_difuso, acumulador_especular);

		}else{

			vec4 res = theLights[i].position - positionEye;
			float d = length(res);
			L = normalize(res.xyz);
			

			if(theLights[i].cosCutOff > 0.0){

				aporteSpot(i, L, N, V, acumulador_difuso, acumulador_especular);

			}else{

				float ate_denom = theLights[i].attenuation[0] + theLights[i].attenuation[1] * d + theLights[i].attenuation[2] * pow(d, 2);

				if (ate_denom > 0.0){
					atenuacion_luz = 1/ate_denom;
				}

				aporte_Posicional(i, L, N, V, acumulador_difuso, acumulador_especular, atenuacion_luz);
				
			}

		}
		

	}
	//Coordenadas de textura que se pasan del vertex-shader al fragment-shader
	f_color = vec4 (scene_ambient + acumulador_difuso + acumulador_especular, 1.0);
	f_texCoord = v_texCoord;
	gl_Position = modelToClipMatrix * vec4(v_position, 1.0);
}
